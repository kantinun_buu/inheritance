/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.inheritance;

/**
 *
 * @author EAK
 */
public class Duck extends Animal{
    protected int numberOfWings;
    public Duck(String name, String color, int numberOfWings){
        super(name, color, 2);
        System.out.println("Duck created");
        this.numberOfWings = numberOfWings;
    }
    public void fly(){
        System.out.println("Duck: " + name + " fly with " + numberOfWings + " Wings.");
    }
    
    @Override
    public void walk(){
        System.out.println("Duck: " + name + " walk with " + numberOfLegs + " legs.");
    }
    
    @Override
    public void speak(){
        System.out.println("Duck: " + name + " speak > Quack Quack!!!");
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kantinun.inheritance;

/**
 *
 * @author EAK
 */
public class TestAnimal {
    public static void main(String[] args) {
        Animal animal = new Animal("Ani", "White", 0);
        animal.speak();
        animal.walk();
        
        Dog dang = new Dog("Dang", "Black&White");
        dang.speak();
        dang.walk();
        
        Cat zero = new Cat("Zero", "Orange");
        zero.speak();
        zero.walk();
        
        Duck som = new Duck("Som", "Orange", 2);
        som.speak();
        som.walk();
        som.fly();
        
        Dog mome = new Dog("Mome", "Black&White");
        mome.speak();
        mome.walk();
        
        Dog to = new Dog("To", "Orange");
        to.speak();
        to.walk();
        
        Dog bat = new Dog("Bat", "Black&White");
        bat.speak();
        bat.walk();
        
        Duck gabgab = new Duck("GabGab", "Black", 2);
        gabgab.speak();
        gabgab.walk();
        gabgab.fly();
        
        System.out.println();
        
        Animal[] animals = {dang, zero, som, mome, to, bat, gabgab};
        for(int i=0; i<animals.length; i++){
            animals[i].walk();
            animals[i].speak();
            if(animals[i] instanceof Duck){
                Duck duck = (Duck)animals[i];
                duck.fly();
            }
        }
    }
}
